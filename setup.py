from setuptools import find_packages, setup


def determine_version(root):
    import setuptools_scm
    version = setuptools_scm.version_from_scm(root)
    if version is None:
        try:
            version = setuptools_scm.get_version()
        except Exception:
            pass
    if version is None:
        version = 'UNKNOWN'
    return version


tests_require = [
   'coverage>5,<6',
   'pylint>2,<3',
]

setup(
    name='agenda',
    use_scm_version=True,
    package_dir={'agenda': ''},
    packages=['agenda'],

    url='https://gitlab.com/Zachu/agenda',
    author='Jani Korhonen',
    author_email='jani@zachu.fi',
    description='Cli translations from sanakirja.org',

    setup_requires=['setuptools_scm'],

    install_requires=[
        'click>=8',
        'icalevents @ git+https://github.com/eigenmannmartin/icalevents.git@f/reccuring-all-day-events#egg=icalevents',
        # 'icalevents>=0.1.26', # 0.1.25 is broken with https://github.com/jazzband/icalevents/issues/71
    ],

    tests_require=tests_require,
    test_suite='tests',

    extras_require={
        'tests': tests_require,
    },

    entry_points={
        'console_scripts': ['agenda = agenda.__main__:cli'],
    },
)
