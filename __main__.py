#!/usr/bin/env python3
import datetime
import netrc
from signal import signal, SIGPIPE, SIG_DFL
import urllib.parse

from zcli.utils import get_config, cache_disk


import click
from httplib2 import Http
import icalevents.icalevents
signal(SIGPIPE, SIG_DFL)


DEFAULT_CONFIG = {
    'calendars': {}
}

@cache_disk(expire=60*10, path_prefix='agenda', serializer='pickle', gzip=True)
def get_calendar_events(url, meta, days_forward=7):
    http = Http()
    domain = urllib.parse.urlparse(url).netloc
    credentials = netrc.netrc().authenticators(domain)
    if credentials:
        http.add_credentials(name=credentials[0], password=credentials[2], domain=domain)
    del credentials

    events = icalevents.icalevents.events(
        url=url,
        start=datetime.datetime.now(),
        end=datetime.datetime.now() + datetime.timedelta(days=days_forward),
        http=http,
    )
    for event in events:
        event.meta = meta.copy()

    return events


@click.command()
def cli():
    calendars = get_config(__package__, defaults=DEFAULT_CONFIG, key='calendars')
    calendar_events = []

    for title, cal in calendars.items():
        cal['title'] = title
        url = cal.pop('url')
        calendar_events += get_calendar_events(url, cal)

    calendar_events.sort(key=lambda x: x.start)
    printed = []
    for event in calendar_events:
        uid = event.uid.split('_')[0]
        if uid in printed:
            continue

        printed.append(uid)
        diff = (event.start - datetime.datetime.now(datetime.timezone.utc)).total_seconds()
        if diff < 0:
            continue
        days = int(diff/60/60//24)
        hours = int(diff/60//60 % 24)
        minutes = int(diff//60 % 60)
        seconds = int(diff//1 % 60)

        diff_str = []
        if days or diff_str:
            diff_str.append(str(days) + 'd')
        if hours or diff_str:
            diff_str.append(str(hours) + 'h')
        if minutes or diff_str:
            diff_str.append(str(minutes) + 'm')
        if seconds or diff_str:
            diff_str.append(str(seconds) + 's')

        click.secho('{} {}: {}'.format(
            event.meta.get('symbol', ''),
            ''.join(diff_str[:2]),
            event.summary,
        ), fg=event.meta.get('color'))


if __name__ == '__main__':
    cli()  # pylint: disable=no-value-for-parameter
